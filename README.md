# Generic observer pattern implementation

This repo contains a reference example of observer pattern implementation using
modern C++.  Please refer to this [blog post](https://twdev.blog/2021/07/observer/) for more details.
